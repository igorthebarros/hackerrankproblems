﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution
{

    // Complete the findMedian function below.
    static int findMedian(int[] arr)
    {
        //Sort the array in ascending order
        Array.Sort(arr);

        //Auxiliary variable
        int result = 0;

        //Test if the array has an odd length
        if (arr.Length % 2 == 1)
        {
            //In this case, the Median it's the center number
            result = (arr.Length / 2);
        }
        else//Otherwise it will be an even length
        {
            //Result gets the half of the array length
            result = arr.Length / 2;
            //The Median will be the sum of the 2 numbers near the center divided by 2
            result = (arr[result - 1] + arr[result]) / 2;
        }
        return arr[result];
    }

    static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine());

        int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp))
        ;

        int result = findMedian(arr);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
