﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution
{

    // Complete the gameOfThrones function below.
    static string gameOfThrones(string s)
    {

        //Declare the array with CHAR size
        int[] cont = new int[256]; 

        //Access the array and counts the times that a letter appears
        for (int i = 0; i < s.Length; i++)
        {
            cont[s[i] - 'a']++;
        }

        //Starts variables to use in the final resolution
        string result = " ";
        int odd = 0;

        for(int i = 0; i < 25; i++)
        {
            if(cont[i] % 2 == 1)
            {
                odd++;
            }
            if(odd > 1)//A palindrome can only have one character that appears an odd number of time
            {
                result = "NO";
            }
            else
            {
                result = "YES";
            }
        }
        return result;
    }

    static void Main(string[] args)
    {
        //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        string result = gameOfThrones(s);

        Console.WriteLine(result);

        //textWriter.WriteLine(result);

        //textWriter.Flush();
        //textWriter.Close();
    }
}
